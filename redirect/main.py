from flask import Blueprint, abort, redirect
from http import HTTPStatus as Status

from redirect.models import Redirect

main = Blueprint('', __name__, url_prefix='/')


@main.route('/')
def index():
    return "Under construction..."


@main.route('/<name>')
def handle_redirect(name):
    try:
        obj = Redirect.get(Redirect.name == name)
    except Redirect.DoesNotExist:
        abort(Status.NOT_FOUND)

    return redirect(obj.dest, code=Status.FOUND)
