import json
import random
import string
from flask import Blueprint, request
from functools import wraps
from http import HTTPStatus as Status

from redirect.models import Redirect, APIKey

api = Blueprint('api', __name__, url_prefix='/api')


class Error:
    API_KEY_REQUIRED = "An API key is required for this endpoint."
    API_KEY_INVALID = "The API key is not valid."

    NAME_REQUIRED = "The 'name' parameter is required."
    DESTINATION_REQUIRED = "The 'destination' parameter is required."

    REDIRECT_EXISTS = "A redirect with that name already exists."
    DOES_NOT_EXIST = "A redirect with that name does not exist."


# Access control


def add_new_api_key():
    key = generate_api_key()
    APIKey.create(key=key)

    return key


def generate_api_key():
    return ''.join(random.choices('0123456789abcdef', k=32))


def get_valid_api_keys():
    for key in APIKey.select(APIKey.key):
        yield key.key


def api_key_required():
    def decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            key = request.headers.get('API-Key', None)

            if key is None:
                return {'success': False, 'error': Error.API_KEY_REQUIRED}, Status.FORBIDDEN
            elif key not in get_valid_api_keys():
                return {'success': False, 'error': Error.API_KEY_INVALID}, Status.FORBIDDEN

            return f(*args, **kwargs)
        return wrapped
    return decorator


# Redirect helper functions


def generate_name(length=5):
    return ''.join(random.choices(string.ascii_lowercase, k=length))


def generate_unique_name(length=5):
    while True:
        name = generate_name(length=length)

        if not redirect_exists(name):
            return name


def redirect_exists(name):
    return Redirect.select().where(Redirect.name == name).exists()


# Routes


@api.route('/redirect', methods=['POST'])
@api_key_required()
def create_redirect():
    data = json.loads(request.data.decode('utf-8'))

    name = data.get('name', '')
    dest = data.get('dest', '')

    if not dest:
        return {'success': False, 'error': Error.DESTINATION_REQUIRED}, Status.BAD_REQUEST

    if not name:
        name = generate_unique_name()
    else:
        if redirect_exists(name):
            return {'success': False, 'error': Error.REDIRECT_EXISTS}, Status.BAD_REQUEST

    Redirect.create(name=name, dest=dest)
    return {'success': True}


@api.route('/redirect', methods=['PATCH'])
@api_key_required()
def update_redirect():
    data = json.loads(request.data.decode('utf-8'))

    name = data.get('name', '')
    dest = data.get('dest', '')

    if not name:
        return {'success': False, 'error': Error.NAME_REQUIRED}, Status.BAD_REQUEST
    if not dest:
        return {'success': False, 'error': Error.DESTINATION_REQUIRED}, Status.BAD_REQUEST

    try:
        redirect = Redirect.get(name=name)
    except Redirect.DoesNotExist:
        return {'success': False, 'error': Error.DOES_NOT_EXIST}, Status.BAD_REQUEST

    redirect.name = name
    redirect.save()
    return {'success': True}


@api.route('/redirect', methods=['DELETE'])
@api_key_required()
def delete_redirect():
    name = request.args.get('name', '')

    if not name:
        return {'success': False, 'error': Error.NAME_REQUIRED}, Status.BAD_REQUEST

    try:
        redirect = Redirect.get(name=name)
    except Redirect.DoesNotExist:
        return {'success': False, 'error': Error.DOES_NOT_EXIST}, Status.BAD_REQUEST

    redirect.delete_instance()
    return {'success': True}
