import datetime
from peewee import BooleanField, CharField, DateTimeField, Model, SqliteDatabase


db_filename = 'redirects.db'
db = SqliteDatabase(db_filename)


def set_up_database():
    db.connect()
    db.create_tables([Redirect, APIKey], safe=True)


class BaseModel(Model):
    class Meta:
        database = db


class Redirect(BaseModel):
    name = CharField(unique=True)
    dest = CharField()


class APIKey(BaseModel):
    key = CharField(unique=True)
    revoked = BooleanField(default=False)
    revoked_on = DateTimeField(null=True)

    def revoke(self):
        self.revoked = True
        self.revoked_on = datetime.datetime.now()
        self.save()
