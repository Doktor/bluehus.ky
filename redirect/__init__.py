from flask import Flask

from redirect.models import set_up_database


def create_app():
    app = Flask(__name__)

    set_up_database()

    from redirect.api import api
    from redirect.main import main
    app.register_blueprint(api)
    app.register_blueprint(main)

    return app
